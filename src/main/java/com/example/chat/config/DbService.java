package com.example.chat.config;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("db-service")
public interface DbService {

    @PostMapping("/send")
    String addMessage(@RequestBody String chatMessage);


    @DeleteMapping("/send")
    String deleteMessage(@RequestParam("id") long id);

}
