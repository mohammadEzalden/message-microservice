package com.example.chat.entities;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChatMessage {

    private String channel;
    private String content;
    private String sender;


    public ChatMessage() {
    }

    public ChatMessage(String type, String content) {
        this.channel = type;
        this.content = content;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    @Override
    public String toString() {
        return "{" +
                "\"channel=\"\"" + channel + '\"' +
                ",\" content=\"\"" + content + '\"' +
                '}';
    }
}